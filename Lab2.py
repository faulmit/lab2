class Army:
	attr_army= "Army"

	def say_hi_army(self):
		print(f"\nI a'm {self.attr_army}")

class Navy(Army):
	attr_navy = "Navy"

	def say_hi_navy(self):
		self.say_hi_army()
		print(f"I a'm {self.attr_navy}")

class Airborne_Forces(Army):
	attr_air = "Airborne Forces"

	def say_hi_airborne(self):
		self.say_hi_army()
		print(f"I a'm {self.attr_air}")

class Space_Forces(Army):
	attr_space = "Space Forces"

	def say_hi_space(self):
		self.say_hi_army()
		print(f"I a'm {self.attr_space}")

class Ground_Forces(Army):
	attr_ground = "Ground Forces"

	def say_hi_ground(self):
		self.say_hi_army()
		print(f"I a'm {self.attr_ground}")

class Underground_Forces(Army):
	attr_under = "Underground Forces"

	def say_hi_under(self):
		self.say_hi_army()
		print(f"I a'm {self.attr_under}")

class Seal(Navy):
	attr_seal = "Seal"

	def say_hi_seal(self):
		self.say_hi_navy()
		print(f"I a'm {self.attr_seal}")

class Hydra(Airborne_Forces):
	attr_hydra = "Hydra"

	def say_hi_hydra(self):
		self.say_hi_airborne()
		print(f"I a'm {self.attr_hydra}")

class Rocket(Space_Forces):
	attr_rocket = "Rocket"

	def say_hi_rocket(self):
		self.say_hi_space()
		print(f"I a'm {self.attr_rocket}")

class Tank(Ground_Forces):
	attr_tank = "Tank"

	def say_hi_tank(self):
		self.say_hi_ground()
		print(f"I a'm {self.attr_tank}")


army = Army()
army.say_hi_army()

navy = Navy()
navy.say_hi_navy()

airborne = Airborne_Forces()
airborne.say_hi_airborne()

space = Space_Forces()
space.say_hi_space()

ground = Ground_Forces()
ground.say_hi_ground()

under = Underground_Forces()
under.say_hi_under()

seal = Seal()
seal.say_hi_seal()

hydra = Hydra()
hydra.say_hi_hydra()

rocket = Rocket()
rocket.say_hi_rocket()

tank = Tank()
tank.say_hi_tank()